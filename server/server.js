//引入express模块
var express = require('express');
//创建web服务器对象
var app =express();
//静态资源处理
app.use(express.static('public'));
app.get('/get',(req,res) => {
    //向客户端响应教程
    res.send('hello,get');
})
app.post('/post',(req,res) => {
    //向客户端响应数据
    res.send('post');
})
app.post('/xml',(req,res) => {
    //向客户端响应数据
    res.send('ok');
})
app.get('/xml',(req,res) => {
    //向客户端响应数据
    res.setHeader('Content-Type','text/xml');
    res.send('<?xml version="1.0" encoding="UTF-8"?><book><name>红楼梦</name><about>曹雪芹</about></book>');
})
app.post('json',(req,res)=>{
    res.send('ok');
})
app.get('/json',(req,res) => {
    var obj = {
        "name":"adierian",
        "age":"16"
       // "anhao":{"anhao":Unknown word.
             //  "book":"人类简史"，
              // "ox":"隆美尔",
          //     "yingy":"("《I live host》"   "yingy":Unknown word. 
    
}
        res.send(obj);
     })
app.get('/cors',(req,res) => {
    res.send('服务器发送成功');
})
//监听3000端口
app.listen(3001, ()=>{
   console.log('服务器启动成功......');
});